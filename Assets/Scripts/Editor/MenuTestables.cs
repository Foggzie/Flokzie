﻿using UnityEditor;
using UnityEngine;

public static class MenuTestables {

	[MenuItem("Quaternions/Print")]
	public static void Quaternions()
	{
		Quaternion a = default(Quaternion);
		Quaternion b;
		b.x = 5f;
		b.y = 6f;
		b.z = 7f;
		b.w = 8f;
		Debug.Log (a);
		Debug.Log (b);
		Debug.Log (b.normalized);
		Debug.Log (a.normalized);
		b.w = 0;
		Debug.Log (b.normalized);
	}
}
