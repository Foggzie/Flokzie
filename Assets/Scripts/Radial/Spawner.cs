﻿using UnityEngine;

namespace RadialFlock {
	using Maths;

	[RequireComponent(typeof(Director))]
	[RequireComponent(typeof(Home))]
	public class Spawner : MonoBehaviour {
		
		Rap rap;
		Director director;

		[SerializeField]
		Actor actorPrefab;

		[SerializeField]
		float polar;

		[SerializeField]
		float azimuthal;

		void Awake() {
			director = GetComponent<Director>();

			rap = new Rap(polar, azimuthal, GetComponent<Home>().Radius);

			AddActor();
		}

		void AddActor() {
			var actorObject = GameObject.Instantiate(actorPrefab.gameObject);
			var actorComponent = actorObject.GetComponent<Actor>();
			actorComponent.Init(rap);
		}

//#if UNITY_EDITOR
//		void OnDrawGizmos() {
//			if (GizmoRapOutdated()) {
//				RebuildGizmoRap();
//			}
//			float x, y, z;
//			gizmoRap.Location(out x, out y, out z);
//			Gizmos.DrawCube(new Vector3(x, y, z), Vector3.one);
//		}
//
//		bool GizmoRapOutdated() {
//			return ((gizmoRap == null) ||
//				(gizmoRap.AzimuthalAngle != azimuthal) ||
//				(gizmoRap.PolarAngle != polar));
//		}
//		//
//		[UnityEditor.Callbacks.DidReloadScripts]
//		void RebuildGizmoRap() {
//			gizmoRap = new Rap(polar, azimuthal, 50f);			
//		}
//
//		private Rap gizmoRap = null;
//#endif
	}
}