﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadialFlock {
	using Maths;
	public class Actor : MonoBehaviour {

		Home home;
		Rap rap;

		public void Init(Rap start) {
			rap = new Rap(start);	
		}

		void LateUpdate() {
			float x, y, z;
			rap.Location(out x, out y, out z);
			if (MovementIsRequired(x, y, z)) {
				transform.localPosition = new Vector3(x, y, z);
			}
		}

		bool MovementIsRequired(float x, float y, float z) {
			return transform.localPosition.x != x ||
			transform.localPosition.y != y ||
			transform.localPosition.z != z;
		}
	}
}
