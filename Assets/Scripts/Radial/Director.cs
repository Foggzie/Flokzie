﻿using System.Collections.Generic;

using UnityEngine;

namespace RadialFlock {
	[RequireComponent(typeof(Home))]
	public class Director : MonoBehaviour {

		float radius;

		HashSet<Actor> actors = new HashSet<Actor>();

		void Awake() {
			radius = GetComponent<Home>().Radius;
		}

		void RegisterActor() {
			
		}

		void StepActors() {
			
		}
	}
}