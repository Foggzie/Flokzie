﻿using UnityEngine;

namespace RadialFlock {
	public class Home : MonoBehaviour {
		
		Transform Visuals {
			get {
				if (visuals == null) {
					Debug.Assert(transform.childCount == 1, "More than one child");
					visuals = transform.GetChild(0);
				}
				return visuals;
			}
		} Transform visuals;

		public float Radius { get { return Visuals.localScale.x / 2; } }

		void Awake() {
			ValidateVisuals();
		}

		void ValidateVisuals() {
			Debug.Assert(Visuals, "No attached visuals");
			Debug.Assert((Visuals.localScale.x == Visuals.localScale.y) &&
				(Visuals.localScale.x == Visuals.localScale.z),
				"Visuals have inconsistent scale");
		}
	}
}