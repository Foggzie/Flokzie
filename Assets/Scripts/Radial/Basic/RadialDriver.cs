﻿//using System;
//using System.Collections.Generic;
//
//using UnityEngine;
//using System.Linq;
//
//namespace Radial
//{
//	using Rand = UnityEngine.Random;
//
//	public delegate void Socializer(RadialDriver driver, ref HashSet<RadialDriver> friends, ref HashSet<RadialDriver> flock);
//
//	public class RadialDriver : MonoBehaviour
//	{
//		public QV _angularVelocity;
//
//		public Vector2 Velocity { get; private set; }
//
//		private void Awake()
//		{
//			socialFrame = Rand.Range(0, 5);
//		}
//
//		public void Initialize(float simluationScale, float boundsRadiusSq, Socializer socializer)
//		{
//			maxSpeed = simluationScale * 1.0f;
//			wanderWeight = simluationScale * 0.2f;
//			accordanceWeight = simluationScale * 0.05f;
//			avoidanceWeight = simluationScale * 1.0f;
//
//			this.boundsRadiusSq = boundsRadiusSq;
//			this.socializer = socializer;
//		}
//
//		public void Step()
//		{
//			StepSocializing();
//			StepFlocking();
//		}
//
//		private void StepSocializing()
//		{
//			socialFrame = (socialFrame + 1) % 5;
//			if (socialFrame == 0)
//			{
//				socializer(this, ref friends, ref flock);
//			}
//		}
//
//		private void StepFlocking()
//		{
//
//
//			var flockDirection = GetFlockDirection();
//			var avoidDirection = GetAvoidDirection();
//			var accordance = GetAccordance();
//			var wander = GetWander();
//
//			_angularVelocity = _angularVelocity + flockDirection + avoidDirection + accordance + bounds + wander;
//			_angularVelocity.aVel = Mathf.Min (_angularVelocity.aVel, maxSpeed);
//
//			transform.Rotate(_angularVelocity);
//		}
//
//		/// <summary>Helps align with the flock.</summary>
//		private QV GetFlockDirection()
//		{
//			QV average = new QV(Quaternion.identity, 0f);
//			return 
//			foreach (var friend in flock.Concat(friends))
//			{
//				var dist = Quaternion.Angle(friend.transform.localRotation, transform.localRotation);
//				average.Add(
//				//+= (friend._angularVelocity.normalized / dist);
//				++count;
//			}
//		}
//
//		/// <summary>Helps prevent a flock from clumping.</summary>
//		private Vector2 GetAvoidDirection()
//		{
//			int count = 0;
//			Vector2 average = Vector2.zero;
//			foreach (var friend in friends)
//			{
//				Vector2 difference = Position - friend.Position;
//				average += difference.normalized / difference.magnitude;
//				++count;
//			}
//
//			average = (count > 1) ? (average / count) : average;
//			return average * avoidanceWeight;
//		}
//
//		/// <summary>Helps a flock stay together.</summary>
//		private Vector2 GetAccordance()
//		{
//			int count = 0;
//			Vector2 average = Vector2.zero;
//			foreach (var friend in flock.Concat(friends))
//			{
//				average += friend.Position;
//				++count;
//			}
//
//			if (count == 0)
//			{
//				return average;
//			}
//			else
//			{
//				average = average / count;
//				Vector2 direction = average - Position;
//				direction.Normalize();
//				return direction * accordanceWeight;
//			}
//		}
//
//		/// <summary>Ensures we dont leave the system.</summary>
//		private Vector2 GetBounds()
//		{
//			var differenceSq = Position.sqrMagnitude - boundsRadiusSq;
//			if (differenceSq > 0)
//			{
//				return -(Position).normalized * (float)Math.Sqrt(differenceSq) * 0.05f;
//			}
//			else
//			{
//				return Vector2.zero;
//			}
//		}
//
//		/// <summary>I am an individual!</summary>
//		private Vector2 GetWander()
//		{
//			return new Vector2(
//				Rand.Range(-wanderWeight, wanderWeight),
//				Rand.Range(-wanderWeight, wanderWeight));
//		}
//
//		private int socialFrame;
//
//		private float maxSpeed;
//		private float wanderWeight;
//		private float avoidanceWeight;
//		private float accordanceWeight;
//		private float boundsRadiusSq;
//
//		private Socializer socializer;
//
//		private HashSet<RadialDriver> friends = new HashSet<RadialDriver>();
//		private HashSet<RadialDriver> flock = new HashSet<RadialDriver>();
//	}
//}
