using UnityEngine;

namespace RadialFlock.Maths {
	[System.Serializable]
	public class Rap {
		public Rap(Rap other) {
			polar = new SmartAngle(other.PolarAngle);
			azimuthal = new SmartAngle(other.AzimuthalAngle);
			radius = other.radius;
		}

		public Rap(float polarRadians, float azimuthalRadians, float homeRadius) {
			polar = new SmartAngle(polarRadians);
			azimuthal = new SmartAngle(azimuthalRadians);
			radius = homeRadius;
		}

		public float radius;

		public float PolarAngle {
			get { return polar.Angle; }
			set { polar.Angle = value; }
		}

		public float AzimuthalAngle {
			get { return azimuthal.Angle; } 
			set { azimuthal.Angle = value; }
		}

		private SmartAngle polar; // [0, pi]
		private SmartAngle azimuthal; // [0, 2pi)

		public void Location(out float x, out float y, out float z) {
			var polarSinByRadius = radius * polar.GetSin();
			x = polarSinByRadius * azimuthal.GetCos();
			y = radius * polar.GetCos();
			z = polarSinByRadius * azimuthal.GetSin();
		}

		public float DifferenceSq(Rap other, float radiusSq) {
			var azDifference = (AzimuthalAngle - other.AzimuthalAngle);
			var poDifference = (PolarAngle - other.PolarAngle);
			return
				(azDifference * azDifference) +
				(poDifference * poDifference) +
				radiusSq;
		}
	}
}
