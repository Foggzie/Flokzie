﻿using UnityEngine;

namespace RadialFlock.Maths {
	public class SmartAngle {
		
		public float Angle { get; set;	}

		public SmartAngle(float start) {
			Angle = start;
			UpdateCos();
			UpdateSin();
		}

		public float GetCos() {
			if (Angle != lastAngleCos) {
				UpdateCos();
			}
			return cos;
		}

		void UpdateCos() {
			cos = Mathf.Cos(Angle);
			lastAngleCos = Angle;
		}

		public float GetSin() {
			if (Angle != lastAngleSin) {
				UpdateSin();
			}
			return sin;
		}

		void UpdateSin() {
			sin = Mathf.Sin(Angle);
			lastAngleSin = Angle;
		}

		float lastAngleCos;
		float lastAngleSin;
		float cos;
		float sin;
	}
}