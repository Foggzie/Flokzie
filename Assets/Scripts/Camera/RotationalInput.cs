﻿using UnityEngine;

public class RotationalInput : MonoBehaviour {
	[SerializeField] private float rotationAcceleration = 0.25f;
	[SerializeField] private float rotationSpeedMax = 2f;
	[SerializeField] private float rotationDamping = 0.95f;

	private float rotationSpeed;

	private void Awake() {
		rotationSpeed = rotationSpeedMax / 4;
	}

	private void Update() { 
		if (Input.GetKey(KeyCode.A)) {
			rotationSpeed += rotationAcceleration;
		}
		if (Input.GetKey(KeyCode.D)) {
			rotationSpeed -= rotationAcceleration;
		}

		rotationSpeed = Mathf.Clamp(rotationSpeed, -rotationSpeedMax, rotationSpeedMax);

		transform.Rotate(0f, rotationSpeed, 0f);

		rotationSpeed *= rotationDamping;
	}
}
